<?php

namespace GranitSDK;

use Phalcon\Cache\Adapter\Libmemcached;
use Phalcon\Storage\SerializerFactory;


class MemCached
{
	private static $instance;

	public static function get() : Libmemcached
	{
		if (!self::$instance) {

			$serializerFactory = new SerializerFactory();

			$options = [
				'defaultSerializer' => 'Php',
				'lifetime'          => Config::get()->getMemCached()->getLifeTime(),
				'servers'           => Config::get()->getMemCached()->getHosts(),
			];

			self::$instance = new Libmemcached($serializerFactory, $options);

		}

		return self::$instance;
	}
}